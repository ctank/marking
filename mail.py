import csv
import os
import glob
import email, smtplib, ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

mails = {}
names = {}

def mail(name, email, filepath, filename, password):
    print("Mailing %s to %s at address %s." % (filepath, name, email))
    subject = "Midterm Script"
    body = "This is your marked midterm script. Please check and get back to Prof. Colin or Prof. Li Wei if there are issues."
    sender_email = "cs1231papers@gmail.com"
    receiver_email = email

    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email

    message.attach(MIMEText(body, "plain"))

    with open(filepath, "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    encoders.encode_base64(part)

    part.add_header(
    "Content-Disposition",
    f"attachment; filename = {filename}"
    )

    message.attach(part)
    text = message.as_string()

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, text)

# Create the Student ID to email mapping
with open("list.csv") as f:
    csvreader = csv.reader(f, delimiter = ',', quotechar = '"')

    for row in csvreader:
        mails[row[1]] = row[2]
        names[row[1]] = row [0]

# Now start mailing out the files

# Create two log files
f = open("mailed.log", "w")
e = open("error.log", "w")

# Go through each directory
password = input("Please enter email password: ")

for dir in glob.glob("T*"):
    print("\n\n===== Processing directory %s ========\n\n" % dir)
    for root, dirs, files in os.walk(dir):
        for file in files:
            parts = file.split(".")

            if len(parts) > 1:
                # Get rid of the tag
                name = parts[0].split('_')
                pname = name[0].strip()

                #print(pname)

                full_name = os.path.join(root, file)

                if pname in mails:
                    f.write("Mailing %s to %s at address %s.\n" % (full_name,
                    mails[pname], names[pname]))

                    mail(names[pname], mails[pname], full_name, file, password)
                    pass
                else:
                    e.write("Cannot find email for %s. pname is %s\n" 
                    %(full_name, pname))
                    pass
f.close()
e.close()

