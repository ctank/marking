import os
import glob
import sys

# Check that user specified who to tag a file with
if len(sys.argv) != 2:
    print("\n\nUsage: %s <tag>\n\n" % sys.argv[0])
    sys.exit(-1)
else:
    tag = sys.argv[1]

# Get the top level dir

for dir in glob.glob("T*"):
    print("\n\n====== DIR: %s =====\n\n" % dir)
    for root, dirs, files in os.walk(dir):
        for file in files:
            parts = file.split('.')

            if len(parts) > 1:
                name = parts[0] + '_' + tag
                for part in parts[1:]:
                    name = name + '.' + part
                print(name)

                # Rename the file
                orig_name = os.path.join(root, file)
                new_name = os.path.join(root, name)

                print("Renaming %s to %s." % (orig_name, new_name))
                os.rename(orig_name, new_name)
